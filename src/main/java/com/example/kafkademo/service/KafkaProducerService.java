package com.example.kafkademo.service;

import com.example.kafkademo.model.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducerService {

    @Autowired
    private KafkaTemplate<String, UserProfile> kafkaTemplate;

    @Value("${kafka.topic}")
    private String kafkaTopic;

    public void sendUserProfile(UserProfile userProfile) {
        kafkaTemplate.send(kafkaTopic, userProfile);
    }
}

package com.example.kafkademo.controller;

import com.example.kafkademo.model.UserProfile;
import com.example.kafkademo.service.KafkaProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Random;

@RestController
public class KafkaController {

    @Autowired
    private KafkaProducerService kafkaProducerService;

    @GetMapping("/generate-profiles")
    public String generateUserProfiles() {
        for (int i = 0; i < 2000; i++) {
            UserProfile userProfile = generateRandomUserProfile();
            kafkaProducerService.sendUserProfile(userProfile);
        }
        return "Pembuatan profil pengguna dimulai.";
    }

    private UserProfile generateRandomUserProfile() {
        String[] names = { "John Doe", "Jane Smith", "David Johnson", "Emily Davis", "Michael Wilson" };
        String[] addresses = { "123 Main St", "456 Elm St", "789 Oak St", "321 Pine St", "654 Maple St" };
        String[] phones = { "1234567890", "0987654321", "1112223333", "4445556666", "7778889999" };

        Random random = new Random();
        int randomIndex = random.nextInt(names.length);
        String name = names[randomIndex];
        String address = addresses[randomIndex];
        String phone = phones[randomIndex];
        int age = random.nextInt(100) + 1;

        return new UserProfile(name, address, phone, age);
    }
}
